# Techsolutions LF6



## Optimierung der Workflow-Struktur und Kundenkommunikation in der IT-Branche:
Implementierung von gewaltfreier Kommunikation, Servicefallklassifizierung und Automatisierungsoptionen

Unsere Aufgabe besteht darin, den aktuellen, unkoordinierten Workflow zu strukturieren und sinnvolle Konzepte zu integrieren. Ein besonderer Schwerpunkt liegt auf der Verbesserung der Kommunikation zwischen Kunden und Support. Hierbei sollen Konzepte der gewaltfreien Kommunikation angewendet werden, um deeskalierend auf Gespräche einzuwirken. Zudem ist eine zuverlässige Einordnung der Servicefälle wichtig, um eine effektivere spätere Bearbeitung zu ermöglichen. Die Geschäftsführung kann sich auch eine Automatisierung des Erstkontakts mit dem Kunden über Chatbots oder ähnliches vorstellen. Inhaltlich wird erwartet, dass die Mitarbeiter sich mit rechtlichen und ethischen Fragen in der IT-Branche auseinandersetzen und angemessene Verhaltensweisen in solchen Situationen kennen.